<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'App\\Http\\Middleware\\Manager'], function () {
    Route::get('create_task', 'TasksController@get_create_form')->name('create.task');
    Route::post('task/save' , 'TasksController@save');
    Route::get('manager/show', 'TasksController@get_assigned_tasks')->name('show.assigned.tasks');
    Route::get('manager/show/unassigned', 'TasksController@get_unassigned_tasks')->name('show.unassigned.tasks');

});
Route::group(['middleware' => 'App\\Http\\Middleware\\Developer'],function () {
   Route::get('developer/show', 'TasksController@get_developer_tasks')->name('show.dev.tasks');
//   Route::post('finished', 'TasksController@update_task')->name('update.task');


});
Route::get('finished', 'TasksController@update_task')->name('update.task');