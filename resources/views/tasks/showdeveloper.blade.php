@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <thead>
            @csrf
            <tr>
                <th>Task Title</th>
                <th>Description</th>
                <th>Created At</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @if(count($dev_tasks) > 0)
                @foreach($dev_tasks as $task)
                    <tr>
                        <td>{{$task->tasks[0]->name}}</td>
                        <td>{{$task->tasks[0]->body}}</td>
                        <td>{{ $task->created_at }}</td>
                        @if($task->status == 0)
                            <td>Pending</td>

                        @else
                            <td>Finished</td>
                        @endif
                        @if($task->status == 0)
                        <td>

                            <a href="#" id="finish" data-id="{{$task->tasks[0]->id}}">
                                <button  type="button" class="btn btn-warning">Finished</button>
                            </a>
                        </td>
                            @endif
                    </tr>

                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">There is no any task yet</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

@endsection

