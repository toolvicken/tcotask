@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ url('task/save') }}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="developer">Developer</label>
                <select multiple class="form-control" id="developer" placeholder="Select Developer" name="developers[]">
                    <option value="">Select Developer</option>
                    @foreach($developers as $developer)
                    <option value="{{$developer->id}}">{{$developer->name}}</option>
                    @endforeach
                </select>

                @if($errors->has('developer'))
                    <p class="v_error">{{ $errors->first('developer') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{ old('name') }}">
                @if($errors->has('name'))
                    <p class="v_error">{{ $errors->first('name') }}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="body">Details:</label>
                <textarea class="form-control" id="body" name="body">{{ old('body') }}</textarea>
                @if($errors->has('body'))
                    <p class="v_error">{{ $errors->first('body') }}</p>
                @endif
            </div>
            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ url('manager/show') }}">
                <button type="button" class="btn btn-warning">Cancel</button>
            </a>

        </form>
    </div>
    @endsection