@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <thead>
            @csrf
            <tr>
                <th>Task Title</th>
                <th>Description</th>
                <th>Developers</th>
                <th>Created At</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @if(count($tasks) > 0)
                @foreach($tasks as $task)
                    <tr>
                        <td>{{ $task->name }}</td>
                        <td>{{ $task->body }}</td>


                        <td>
                            @foreach($task->developers as $developer)
                            {{$developer->name }},
                        @endforeach
                        </td>
                        <td>{{ $task->created_at }}</td>
                        @if($task->status == 0)
                        <td>Pending</td>
                        @else
                            <td>Finished</td>
                        @endif
                    </tr>

                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">There is no any task yet</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

    @endsection