<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';



    public function developers(){

    return $this->belongsToMany(User::class, 'developers_tasks');

    }

}
