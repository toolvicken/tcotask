<?php

namespace App\Http\Controllers;

use App\DeveloperTask;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function get_assigned_tasks()
    {

        $user_id = auth()->user()->id;
        $tasks = Task::with(['developers'])->whereHas('developers')->where('user_id', '=', $user_id)->get();


        return view('tasks.show')->with(compact('tasks'));


    }

    public function get_unassigned_tasks(){
        $user_id = auth()->user()->id;
        $tasks = Task::doesntHave('developers')->where('user_id', '=', $user_id)->get();

        return view('tasks.show')->with(compact('tasks'));


    }

    public function get_create_form()
    {

        $developers = User::where('role', '=', '1')->get();
        return view('tasks.create')->with(compact('developers'));

    }

    public function save(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            'body' => 'required',
        ]);

        $task = new Task();

        $task->user_id = auth()->user()->id;
        $task->name = $request->input('name');
        $task->body = $request->input('body');

        $task->save();



        if ($request->developers != null) {

            foreach ($request->developers as $developer) {

                $developer_task = new DeveloperTask();

                $developer_task->user_id = $developer;
                $developer_task->task_id = $task->id;

                $developer_task->save();
            }
        }


        return redirect()->route('home');
    }

    public function get_developer_tasks()
    {
        $user_id = auth()->user()->id;

        $dev_tasks = User::with('tasks')->where('id', '=', $user_id)->get();

        return view('tasks.showdeveloper')->with(compact('dev_tasks'));

    }

    public function update_task(Request $request)
    {

        dd('reahced');
    }




}
